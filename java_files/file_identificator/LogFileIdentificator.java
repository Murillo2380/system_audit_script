package java_files.file_identificator;

import java.util.*;
import java.io.File;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

/**
 * Reads the log file and try to identify to which 
 * program the log file belogs to.
 * @author Murillo Henrique Pedroso Ferreira
 * @see #getLogType(File)
 */
public class LogFileIdentificator{

	/**
	 * Returned by @link{#getLogType(File)} if an error occur
	 * while trying to read the given file.
	 */
	public static final int READ_ERROR = -2;

	/**
	 * Returned by @link{#getLogType(File)} when
	 * the given file belogs is {@code null}.
	 */
	public static final int NULL_FILE = -1;

	/**
	 * Returned by @link{#getLogType(File)} when
	 * the given file belogs to an unknown program.
	 */
	public static final int LOG_UNKNOWN = 0;

	/**
	 * Returned by @link{#getLogType(File)} when
	 * the given file belogs to <b>RKHunter</b>
	 */
	public static final int LOG_RKHUNTER = 1;

	/**
	 * Returned by @link{#getLogType(File)} when
	 * the given file belogs to <b>LogWatch</b>
	 */
	public static final int LOG_LOGWATCH = 2;

	/**
	 * Returned by @link{#getLogType(File)} when
	 * the given file belogs to <b>Lynis</b>
	 */
	public static final int LOG_LYNIS = 3;
	
	/**
	 * Returned by @link{#getLogType(File)} when
	 * the given file belogs to <b>ps</b>
	 */
	public static final int LOG_PS = 4;

	/**
	 * Identify the given log file
	 * @param file Log file to be identifyed
	 * @return The code to which program the given log file 
	 *         belogs to.
	 * @see #READ_ERROR
	 * @see #NULL_FILE
	 * @see #LOG_UNKNOWN
	 * @see #LOG_RKHUNTER
	 * @see #LOG_LOGWATCH
	 * @see #LOG_LYNIS
	 */
	public int getLogType(File file){

		if(file == null)
			return NULL_FILE;
			
		String firstLine;
		
		try{
			
			BufferedReader br = new BufferedReader(new FileReader(file));
			
			// Skip empty lines that might be at the beginning of 
			// the log file (like in logwatch log file)
			do{
				firstLine = br.readLine();
			}while(firstLine.isEmpty());
			
			br.close();
		
		}catch(IOException e){
			System.out.println("Error while reading " 
				+ file.getAbsolutePath() + file.getName());
			System.out.println("\tError: " + e.getMessage());
			
			return READ_ERROR;
		}


		if(isLynisLog(firstLine))
			return LOG_LYNIS;
		
		if(isLogWatchLog(firstLine))
			return LOG_LOGWATCH;
			
		if(isRKHunterLog(firstLine))
			return LOG_RKHUNTER;

		if(isPSLog(firstLine))
			return LOG_PS;

		return LOG_UNKNOWN;
	}

	private boolean isLynisLog(String firstLine){
		
		if(firstLine == null)
			return false;

		return firstLine.contains("Lynis");
	}
	
	private boolean isLogWatchLog(String firstLine){

		if(firstLine == null)
			return false;

		return firstLine.contains("Logwatch");
	}

	private boolean isRKHunterLog(String firstLine){

		if(firstLine == null)
			return false;
			
		return firstLine.contains("Rootkit Hunter");
	}
	
	private boolean isPSLog(String firstLine){

		if(firstLine == null)
			return false;
			
		return firstLine.matches("\\s+PID\\s+TTY\\s+STAT\\s+TIME\\s+COMMAND");
		
	}

}
