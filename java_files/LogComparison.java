package java_files;

import java.util.*;
import java.io.File;
import java_files.program_comparison.*;
import java_files.file_identificator.*;

public abstract class LogComparison{

	private static final int EXIT_CODE_ERR_NO_ARGS = 100;

	public static void main(String[] args){
	
		LogFileIdentificator fileIdentificator = new LogFileIdentificator();

		String rkhunterLogFilePath1 = null;
		String rkhunterLogFilePath2 = null;
		
		String lynisReportFilePath1 = null;
		String lynisReportFilePath2 = null;
		
		String psLogFilePath1 = null;
		String psLogFilePath2 = null;

		if(args.length == 0){ // print help message

			System.out.println("Supply 2 files of at least one of the following programs:");
			System.out.println("\t- RKHunter log file");
			System.out.println("\t- LogWatch log file");
			System.out.println("\t- Lynis report file");
			System.out.println();
			System.out.println("Log/report files will be automatically detected.");
			System.out.println();
			System.out.println("The left most file is considered to be \"old\" "
			+ ", generated before a suspicious event.");
			System.out.println();
			System.out.println("Program call example:");
			System.out.println("$ java LogComparison lynis_report_file_before_event.dat"
			+ " lynis_report_file_after_event.dat"
			+ " rkhunter_report_file_before_event.log"
			+ " rkhunter_report_file_after_event.log");
			System.out.println(System.lineSeparator());
			
			System.exit(EXIT_CODE_ERR_NO_ARGS);
		}
		
		for(String arg : args){		
		
			switch(fileIdentificator.getLogType(new File(arg))){
			
				case LogFileIdentificator.LOG_RKHUNTER:

					System.out.println("Found rkhunter log file: " + arg);

					if(rkhunterLogFilePath1 == null)
						rkhunterLogFilePath1 = arg;
					
					else 
						rkhunterLogFilePath2 = arg;

					break;

				case LogFileIdentificator.LOG_LOGWATCH:
						
					System.out.println("Found logwatch log file: " + arg);
						
					break;

				case LogFileIdentificator.LOG_LYNIS:
					
					System.out.println("Found lynis report file: " + arg);
					
					if(lynisReportFilePath1 == null) 
						lynisReportFilePath1 = arg;
					else 
						lynisReportFilePath2 = arg;
						
					break;
					
				case LogFileIdentificator.LOG_PS:
					
					System.out.println("Found ps log file: " + arg);
					
					if(psLogFilePath1 == null) 
						psLogFilePath1 = arg;
					else 
						psLogFilePath2 = arg;
					
					break;
					
				default:
					System.out.println("Unknown log file:" + arg);
					
			}
			
					
		}
		
		// TODO check which file is older
		if(rkhunterLogFilePath1 != null && rkhunterLogFilePath2 != null){
			System.out.println();
			System.out.println("Comparing rkhunter log files "
				+ rkhunterLogFilePath1 + " and " + rkhunterLogFilePath2);
				
			new RKHunterComparison().compareLogs(
				new File(rkhunterLogFilePath1), 	
				new File(rkhunterLogFilePath2));
		}
		
		// TODO check which file is older
		if(lynisReportFilePath1 != null && lynisReportFilePath2 != null){
			System.out.println();
			System.out.println("Comparing lynis report files "
				+ lynisReportFilePath1 + " and " + lynisReportFilePath2);
				
			new LynisComparison().compareLogs(
				new File(lynisReportFilePath1), 	
				new File(lynisReportFilePath2));
		}
		
		// TODO check which file is older
		if(psLogFilePath1 != null && psLogFilePath2 != null){
			System.out.println();
			System.out.println("Comparing ps log files "
				+ psLogFilePath1 + " and " + psLogFilePath2);
				
			new PSComparison().compareLogs(
				new File(psLogFilePath1), 	
				new File(psLogFilePath2));
		}
		
	}
	
	/**
	 * Compare both log files to check if the overall system secutiry has changed
	 * @param oldLog Log holding the information of the system prior to running something
	 * @param newLog Log holding the information of the system after running something
	 * @return {@code true} if any difference has been found between the given log files
	 */
	public abstract boolean compareLogs(File oldLog, File newLog);

}	
