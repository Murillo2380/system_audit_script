package java_files.program_comparison;

import java.util.*;
import java_files.LogComparison;
import java.io.File;
import java.io.FileReader;
import java.io.BufferedReader;
import java.io.IOException;
import java.lang.Override;

/**
 * <p>Class that compares two rkhunter log files, searching for entries that are in one report
 * but is not in the other report or its status is different. This class considers that there is an "old" log file
 * that has been generated before an event, and a "new" report file that has been generated
 * after that event.</p>
 * <p>If any big changes is found, that event may be considered as suspicious.</p>
 * <p>The following changes is being sought:</p>
 * <ul>
 * 	<li>Entries that had its status changed</li>
 * 	<li>New entries</li>
 * </ul>
 * <p>An entry is considered to have the following format:</p>
 * <p>    [HH:MM:SS] check info [ STATUS ]</p>
 * <p>The following regex expression is being used to capture those lines:</p>
 * <p>    (.*)\[ ((Warning)|(None found)|(Found)|(Not found)|(OK)) \](.*)</p>
 * @author Murillo Henrique Pedroso Ferreira
 */
public class RKHunterComparison extends LogComparison{

	/**
	 * Holds the system status in the first log to be compared with the second log
	 */
	private Map<String, LogStatusHolder> systemStatus;

	public RKHunterComparison(){
		
		systemStatus = new HashMap<>(4000 /* Big initial capacity */ );
	}
	
	@Override
	public boolean compareLogs(File oldLog, File newLog){

		if(oldLog == null || newLog == null){
			System.out.println(this.getClass().getName() + ": error, log file cannot "
			+ "be null"); // TODO print a more user-friendly error message
			return false;
		}
		
		boolean changesFound = false;
		
		if(mapSystemStatus(oldLog))
			changesFound = compareSystemStatus(oldLog, newLog);
		
		// release the memory, big log files may consume a lot of memory
		systemStatus.clear();
		
		return changesFound;
		
	}
	
	/**
	 * <p>Reads the {@code oldLog} file and map every status that has been found.</p>
	 * <p>Considering the RKHunter 1.4.4, each status has the following format:</p>
	 * <p>[HH:MM:SS] what has been checked [ Status ]</p>
	 * <p>"what has been checked" will be the key entry to the map {@link #systemStatus},
	 * holding the status for each entry </p>
	 * @return {@code true} if the mapping has been ran without any errors
	 */
	private boolean mapSystemStatus(File oldLog){

		try{
			BufferedReader br = new BufferedReader(new FileReader(oldLog));
			String line;
			int currentLine = 0;
			
			while((line = br.readLine()) != null){
		
				String[] logInfo = getCheckMessageAndStatus(line);
				currentLine++;
				
				if(logInfo == null)
					continue; // ignore line
				
					
				// System.out.println("Line = " + logInfo[0]);
				// System.out.println("Extracted = " + logInfo[0] + " Status = " + logInfo[1]);
				
				systemStatus.put(logInfo[0], new LogStatusHolder(currentLine, logInfo[1]));
				
			}
			
			br.close();
		}catch(IOException e){
			System.out.println("Failed to read " + oldLog.getAbsolutePath()
				+ oldLog.getName());
			System.out.println("\tError: " + e.getMessage());
			
			return false;
		}
		
		return true;
	}
	
	/**
	 * Compares each entry of each check info from the {@code newLog},
	 * printing in the stdout if any difference is found (changed status or new check info)
	 * that was not mapped during the exectution of @link{#mapSystemStatus(File)}.
	 * @param oldLog File which the function @link{#mapSystemStatus(File)} did the map.
	 * @param newLog File to be compared
	 * @return {@code true} if any difference has been found.
	 */
	private boolean compareSystemStatus(File oldLog, File newLog){
	
		try{
			BufferedReader br = new BufferedReader(new FileReader(newLog));
			String line;
			
			String oldLogFileName = oldLog.getName();
			String newLogFileName = newLog.getName();
			
			int currentLine = 0;
			boolean differencesFound = false;
			
			while((line = br.readLine()) != null){
		
				String[] logInfo = getCheckMessageAndStatus(line);
				currentLine++;
								
				if(logInfo == null)
					continue; // ignore line
					
				LogStatusHolder mappedStatus = systemStatus.get(logInfo[0]);
				
				// compare mapped status to the status that has been just read
				
				if(mappedStatus == null){
				
					System.out.println("New entry in " + newLogFileName 
						+ ":" + currentLine + System.lineSeparator()
						+ "\t- " + logInfo[0] + System.lineSeparator()
						+ "\t- Status = " + logInfo[1]);
					System.out.println();
				
				}
				
				else if(!mappedStatus.status.equals(logInfo[1])){

					differencesFound = true;

					System.out.println("Check info: " + logInfo[0]);
					System.out.println("\t- Old status (in " + oldLogFileName 
						+ ":" + mappedStatus.logLine + "): " + mappedStatus.status);
					System.out.println("\t- New status (in " + newLogFileName 
						+ ":" + currentLine + "): " + logInfo[1]);
					System.out.println();

				}
				
			}
			
			br.close();
			
			if(!differencesFound)
				System.out.println("System integrity checked by the "
				+ "rkhunter is the same in both log files:"
				+ System.lineSeparator()
				+ "\t- " + oldLog.getName()
				+ System.lineSeparator()
				+ "\t- " + newLog.getName());
				
			return differencesFound;
			
		}catch(IOException e){
			System.out.println("Failed to read " + oldLog.getAbsolutePath()
				+ oldLog.getName());
			System.out.println("\tError: " + e.getMessage());			
			
			return false;
		}

	}
	
	/**
	 * Recalling the structure of the log stated in {@link #mapSystemStatus(File)},
	 * extract the info and status from the log line if the line matches
	 * the following regex: (.*)\[ ((Warning)|(None found)|(Found)|(Not found)|(OK)) \](.*)"
	 *
	 * @return {@code null} if the {@code logLine} does not match the regex or it is null, 
	 * otherwise returns the info and status at the index {@code 0} and {@code 1} respectively.
	 */
	private String[] getCheckMessageAndStatus(String logLine){
	
		if(logLine == null)
			return null;
		
		// considered RKHunter version 1.4.4
		// Looking for something like: [date] info text [ Warning ]
		if(!logLine.matches("(.*)\\[ ((Warning)|(None found)|(Found)|(Not found)|(OK)) \\](.*)"))
			return null;

		int firstClosingSquareBracketIndex = logLine.indexOf(']');
		int lastOpeningSquareBracketIndex = logLine.lastIndexOf('[');
		int lastClosingSquareBracketIndex = logLine.lastIndexOf(']');
		
		String info = logLine.substring(firstClosingSquareBracketIndex + 1, 
			lastOpeningSquareBracketIndex).trim();
		String status = logLine.substring(lastOpeningSquareBracketIndex + 1,
			lastClosingSquareBracketIndex).trim();
			
		return new String[]{info, status};
	
	}
	
	/**
	 * Simple holder that keep the information of the status and file line
	 * of a mapped rkhunter check.
	 */
	private class LogStatusHolder{
	
		final int logLine;
		final String status;
		
		/**
		 * @param logLine Line of the log to be held.
		 * @param status Status held.
		 */
		LogStatusHolder(int logLine, String status){
			this.logLine = logLine;
			this.status = status;
		}
	
	}

}

