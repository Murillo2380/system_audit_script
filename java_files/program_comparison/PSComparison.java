package java_files.program_comparison;

import java.util.*;
import java_files.LogComparison;
import java.io.File;
import java.io.FileReader;
import java.io.BufferedReader;
import java.io.IOException;
import java.lang.Override;

/**
 * <p>Class that compares two ps log files, searching for entries that are in one log
 * but is not in the other. This class considers that there is an "old" log file
 * that has been generated before an event, and a "new" report file that has been generated
 * after that event.</p>
 * <p>If any big changes is found, that event may be considered as suspicious.</p>
 * <p>The following changes is being sought:</p>
 * <ul>
 * 	<li>Any process that were left running after the event</li>
 * </ul>
 * <p>An entry is considered to have the following format:</p>
 * @author Murillo Henrique Pedroso Ferreira
 */
public class PSComparison extends LogComparison{

	/**
	 * Maps the process name to the amount of process with the same name that
	 * may be running at the same time.
	 */
	private Map<String, Integer> runningProcesses;

	public PSComparison(){
		
		runningProcesses = new HashMap<>(4000 /* Big initial capacity */ );
	}
	
	@Override
	public boolean compareLogs(File oldLog, File newLog){

		if(oldLog == null || newLog == null){
			System.out.println(this.getClass().getName() + ": error, log file cannot "
			+ "be null"); // TODO print a more user-friendly error message
			return false;
		}
		
		boolean changesFound = false;
		
		if(mapSystemStatus(oldLog))
			changesFound = compareSystemStatus(oldLog, newLog);
		
		return changesFound;
		
	}
	
	/**
	 * <p>Reads the {@code oldLog} file and map every status that has been found.</p>
	 * @return {@code true} if the mapping has been ran without any errors
	 */
	private boolean mapSystemStatus(File oldLog){

		try{
			BufferedReader br = new BufferedReader(new FileReader(oldLog));
			String line;
			
			br.readLine(); // ignore header
			
			while((line = br.readLine()) != null){
		
				// Line structure: (.)*\d\d:\d\d <process_name>
				// Finds the first space after :
				String processName = line.substring(line.indexOf(' ', 
					line.indexOf(':'))).trim();
				
				Integer currentValue = runningProcesses.get(processName);
				runningProcesses.put(processName,
					 (currentValue == null ? 0 : currentValue) + 1);
				
			}
			
			br.close();
		}catch(IOException e){
			System.out.println("Failed to read " + oldLog.getAbsolutePath()
				+ oldLog.getName());
			System.out.println("\tError: " + e.getMessage());
			
			return false;
		}
		
		return true;
	}
	
	/**
	 * Compares each entry of each check info from the {@code newLog},
	 * printing in the stdout if any difference is found (new program running)
	 * that was not mapped during the exectution of @link{#mapSystemStatus(File)}.
	 * @param oldLog File which the function @link{#mapSystemStatus(File)} did the map.
	 * @param newLog File to be compared
	 * @return {@code true} if any difference has been found.
	 */
	private boolean compareSystemStatus(File oldLog, File newLog){
	
		try{
			BufferedReader br = new BufferedReader(new FileReader(newLog));
			String line;
			
			boolean differencesFound = false;

			br.readLine(); // ignore header
			
			while((line = br.readLine()) != null){
		
				// Line structure: (.)*\d\d:\d\d <process_name>
				// Finds the first space after :
				String processName = line.substring(line.indexOf(' ', 
					line.indexOf(':'))).trim();
				
				// compare mapped status to the status that has been just read
				Integer processCountInteger = runningProcesses.get(processName);
				// avoid NullPointerException
				int processCount = processCountInteger == null ? 0 : processCountInteger;
				
				if(processCount > 0) // remove one process match
					runningProcesses.put(processName, --processCount);

				else if(processCount == 0){	
					differencesFound = true;
					System.out.println("Found new process running in " 
						+ newLog.getName() 
						+ System.lineSeparator()
						+ System.lineSeparator()
						+ "\t- " + processName 
						// get first column of the line
						+ " (PID " + line.split("\\s", 2)[0] + ")" 
						+ System.lineSeparator());
				
				}	
				
			}
			
			br.close();
			
			if(!differencesFound)
				System.out.println("Processes running shown by "
				+ "ps are the same in both log files:"
				+ System.lineSeparator()
				+ "\t- " + oldLog.getName()
				+ System.lineSeparator()
				+ "\t- " + newLog.getName());
				
			return differencesFound;
			
		}catch(IOException e){
			System.out.println("Failed to read " + oldLog.getAbsolutePath()
				+ oldLog.getName());
			System.out.println("\tError: " + e.getMessage());			
			
			return false;
		}

	}

}

