package java_files.program_comparison;

import java.util.*;
import java_files.LogComparison;
import java.io.File;
import java.io.FileReader;
import java.io.BufferedReader;
import java.io.IOException;
import java.lang.Override;

/**
 * <p>Class that compares two lynis report files, searching for entries that are in one report
 * but is not in the other report. This class considers that there is an "old" report file
 * that has been generated before an event, and a "new" report file that has been generated
 * after that event.</p>
 * <p>If any big changes is found, that event may be considered as suspicious.</p>
 * <p>The following changes is being sought:</p>
 * <ul>
 * 	<li>New boot services</li>
 * 	<li>Changes in the kernel module</li>
 * 	<li>New cronjobs</li>
 * 	<li>Recently installed packages</li>
 * </ul>
 * @author Murillo Henrique Pedroso Ferreira
 */
public class LynisComparison extends LogComparison{


	private ArrayList<String> bootServices;
	private ArrayList<String> loadedKernelModules;
	private ArrayList<String> cronJobs;
	private ArrayList<String> installedPackages;

	public LynisComparison(){
		
		// Big initial capacity 
		cronJobs = new ArrayList<>(4000);
		bootServices = new ArrayList<>(4000);
		installedPackages = new ArrayList<>(4000);
		loadedKernelModules = new ArrayList<>(4000);
		
	}
	
	@Override
	public boolean compareLogs(File oldLog, File newLog){

		if(oldLog == null || newLog == null){
			System.out.println(this.getClass().getName() + ": error, log file cannot "
			+ "be null"); // TODO print a more user-friendly error message
			return false;
		}
		
		boolean changesFound = false;
		
		if(mapSystemStatus(oldLog))
			changesFound = compareSystemStatus(oldLog, newLog);
		
		return changesFound;
		
	}
	
	/**
	 * <p>Reads the {@code oldLog} file and map every boot services, cronjobs, 
	 * kernel modules and installed packages.</p>
	 * @return {@code true} if the mapping has been ran without any errors
	 */
	private boolean mapSystemStatus(File oldLog){

		try{
			BufferedReader br = new BufferedReader(new FileReader(oldLog));
			String line;
			
			while((line = br.readLine()) != null){

				// Stores the program name that runs after boot
				if(line.startsWith("boot_service[]=")){ 

					line = line.substring(line.indexOf('=') + 1).trim();
					bootServices.add(line);

				}				

				else if(line.startsWith("loaded_kernel_module[]=")){

					line = line.substring(line.indexOf('=') + 1).trim();
					loadedKernelModules.add(line);

				}
				
				else if(line.startsWith("installed_packages_array=")){

					line = line.substring(line.indexOf('=') + 1);
					
					String packages[] = line.split("\\|");
					
					for(String p : packages)
						installedPackages.add(p);
					
					

				}
				
				else if(line.startsWith("cronjob[]=")){

					line = line.substring(line.indexOf('=') + 1).trim();
					cronJobs.add(line);

				}		
				
				
				
			}
			
			br.close();
		}catch(IOException e){
			System.out.println("Failed to read " + oldLog.getAbsolutePath()
				+ oldLog.getName());
			System.out.println("\tError: " + e.getMessage());
			
			return false;
		}
		
		return true;
	}
	
	/**
	 * Searches for new entries that has been saved during the execution of 
	 * @link{#mapSystemStatus(File)}, printing in the stdout if any new entry is found.
	 * @param oldLog File which the function @link{#mapSystemStatus(File)} did the map.
	 * @param newLog File to be compared
	 * @return {@code true} if any difference has been found.
	 */
	private boolean compareSystemStatus(File oldLog, File newLog){
	
	
		try{
			BufferedReader br = new BufferedReader(new FileReader(newLog));
			String line;
			boolean changesFound = false;
			int currentLine = 0;
			
			Collections.sort(cronJobs);
			Collections.sort(bootServices);
			Collections.sort(installedPackages);
			Collections.sort(loadedKernelModules);
			
			while((line = br.readLine()) != null){

				currentLine++;

				if(line.startsWith("boot_service[]=")){
	
					line = line.substring(line.indexOf('=') + 1).trim();

					if(Collections.binarySearch(bootServices, line) < 0){ // not in the arraylist
						System.out.println("New boot service has been found"
						+ " in " + newLog.getName() + ":" + currentLine);
						System.out.println("\t-" + line);
						System.out.println();
						changesFound = true;
					}
					
				}				

				else if(line.startsWith("loaded_kernel_module[]=")){
				
					line = line.substring(line.indexOf('=') + 1).trim();

					if(Collections.binarySearch(loadedKernelModules, line) < 0){ // not in the arraylist
						System.out.println("New loaded kernel module has been found"
						+ " in " + newLog.getName() + ":" + currentLine);
						System.out.println("\t-" + line);
						System.out.println();
						changesFound = true;
					}

				}
				
				else if(line.startsWith("installed_packages_array=")){

					line = line.substring(line.indexOf('=') + 1).trim();
					
					String[] packages = line.split("\\|");
					
					for(String p : packages)

						if(Collections.binarySearch(installedPackages, p) < 0){ // not in the arraylist
							System.out.println("New installed package has been found"
							+ " in " + newLog.getName() + ":" + currentLine);
							System.out.println("\t-" + p);
							System.out.println();
							changesFound = true;
						}

				}
				
				else if(line.startsWith("cronjob[]=")){

					line = line.substring(line.indexOf('=') + 1).trim();

					if(Collections.binarySearch(cronJobs, line) < 0){ // not in the arraylist
						System.out.println("New cronjob has been found"
						+ " in " + newLog.getName() + ":" + currentLine);
						System.out.println("\t-" + line);
						System.out.println();
						changesFound = true;
					}

				}		
				
				
				
			}
			
			if(!changesFound)
				System.out.println("System integrity checked by the "
				+ "lynis is the same in both log files:"
				+ System.lineSeparator()
				+ "\t- " + oldLog.getName()
				+ System.lineSeparator()
				+ "\t- " + newLog.getName());
			
			br.close();
			
			return changesFound;
			
		}catch(IOException e){
			System.out.println("Failed to read " + oldLog.getAbsolutePath()
				+ oldLog.getName());
			System.out.println("\tError: " + e.getMessage());
			
			return false;
		}
		
	}

}

